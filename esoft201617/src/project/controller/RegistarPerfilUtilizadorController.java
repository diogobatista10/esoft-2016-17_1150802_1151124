/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.controller;

import project.model.CentroEventos;
import project.model.Utilizador;

/**
 *
 * @author diogo
 */
public class RegistarPerfilUtilizadorController {

    private CentroEventos ce;
    private Utilizador utilizador;

    /**
     * Construtor
     *
     * @param ce Centro Eventos
     * @param utilizador Utilizador
     */
    public RegistarPerfilUtilizadorController(CentroEventos ce, Utilizador utilizador) {
        this.ce = ce;
        this.utilizador = utilizador;
    }

    /**
     * Cria um novo Utilizador
     *
     * @return novo utilizador
     */
    public Utilizador novoUtilizador() {
        return ce.novoUtilizador();
    }

    /**
     * Modifica os dados do utilizador
     *
     * @param username username
     * @param email email
     * @param password password
     * @param nome nome
     */
    public void setDados(String username, String email, String password, String nome) {
        utilizador.setUsername(username);
        utilizador.setNome(nome);
        utilizador.setEmail(email);
        utilizador.setPassword(password);
    }

    /**
     * Valida o utilizador
     *
     * @return true caso valide ou false
     */
    public boolean validaUtilizador() {
        boolean valida = utilizador.valida();
        return valida;

    }

    /**
     * Regista o utilizador
     *
     * @return true caso registe ou false
     */
    public boolean registaUtilizador() {
        return ce.registaUtilizador(utilizador);
    }

}

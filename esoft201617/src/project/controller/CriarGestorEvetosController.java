/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.controller;

import java.util.List;
import project.model.CentroEventos;
import project.model.Gestor;
import project.model.Utilizador;

/**
 *
 * @author carlo
 */
public class CriarGestorEvetosController {
  private CentroEventos ce;

    public CriarGestorEvetosController(CentroEventos ce) {
        this.ce = ce;
    }

    public List<Utilizador> getListaUtilizadores() {
        return ce.getListaUtilizadores();
    }
     public Gestor novoGestor(Utilizador u)   {
     return ce.novoGestor(u);
     }  
     public boolean registaGestor(Gestor g){
         return ce.registaGestor(g);
     }
}

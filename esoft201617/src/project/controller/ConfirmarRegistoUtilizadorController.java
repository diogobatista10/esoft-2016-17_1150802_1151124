/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.controller;

import java.util.List;
import project.model.CentroEventos;
import project.model.Utilizador;

/**
 *
 * @author diogo
 */
public class ConfirmarRegistoUtilizadorController {

    private CentroEventos ce;

    /**
     * Construtor Vazio
     */
    public ConfirmarRegistoUtilizadorController() {
    }

    /**
     * Construtor Completo
     *
     * @param ce Centro Eventos
     */
    public ConfirmarRegistoUtilizadorController(CentroEventos ce) {
        this.ce = ce;
    }

    /**
     * Retorna lista de utilizadores por confirmar
     *
     * @return lista utilizadores por confirmar
     */
    public List<Utilizador> getListaUtilizadoresPorConfirmar() {
        return ce.getListaUtilizadoresPorConfirmar();
    }

    /**
     * Retorna utilizador
     *
     * @param u utilizador
     * @return utilizador
     */
    public Utilizador getUtilizador(Utilizador u) {
        return ce.getUtilizador(u);
    }

    /**
     * Confirma o utilizador
     *
     * @param u utilizador
     */
    public void confirmaUtilizador(Utilizador u) {
        ce.confirmarUtilizador(u);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.controller;

import java.util.List;
import project.model.Candidatura;
import project.model.CentroEventos;
import project.model.Decisao;
import project.model.Evento;
import project.model.FAE;
import project.model.Organizador;

/**
 *
 * @author diogo
 */
public class AtribuirCandidaturasFAEController {

    private CentroEventos ce;
    private Evento e;

    /**
     * Construtor completo
     *
     * @param ce Centro Eventos
     * @param e Evento
     */
    public AtribuirCandidaturasFAEController(CentroEventos ce, Evento e) {
        this.ce = ce;
        this.e = e;
    }

    /**
     * Construtor vazio
     */
    public AtribuirCandidaturasFAEController() {
    }

    /**
     * Retorna lista eventos
     *
     * @param o organizador
     * @return lista eventos
     */
    public List<Evento> getListaEventos(Organizador o) {
        return ce.getListaEventosOrganizador(o);
    }

    /**
     * Retorna lista candidaturas
     *
     * @param e evento
     * @return lista candidaturas
     */
    public List<Candidatura> getListaCandidaturas(Evento e) {
        return e.getListaCandidaturas();
    }

    /**
     * Retorna lista fae
     *
     * @param e evento
     * @return lista faes
     */
    public List<FAE> getListaFAEs(Evento e) {
        return e.getListaFAE();
    }

    /**
     * Cria uma nova decisão
     *
     * @return nova decisao
     */
    public Decisao novaDecisao() {
        return e.novaDecisao();
    }

    /**
     * Regista decisão
     *
     * @param e Evento
     * @return true caso registado
     */
    public boolean registaDecisao(Evento e) {
        return e.registaDecisao();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.controller;

import java.util.List;
import project.model.CentroEventos;
import project.model.Congresso;
import project.model.Perito;
import project.model.Tema;
import project.model.Workshop;

/**
 *
 * @author carlo
 */
public class CriarWorkshopController {

    private CentroEventos ce;

    public CriarWorkshopController(CentroEventos ce) {
        this.ce = ce;
    }

    public CriarWorkshopController() {
    }

    public List<Congresso> getCongressosDisponiveis() {

        return ce.getListaCongressos();
    }

    public Workshop novoWorkshop(Congresso c) {

        return c.novoWorkshop();
    }

    public boolean setDados(Workshop w, String codigo, String descricao) {
        return w.setDados(codigo, descricao);
    }

    public Tema novoTema(Workshop w) {

        return w.novoTema();
    }

    public Perito novoPerito(Workshop w) {
        return w.novoPerito();
    }

    public boolean validaWorkshop(Workshop w) {
        return w.valida();
    }
    
    public boolean registaWorkshop(Congresso c,Workshop w){
    return c.add(w);
    }
}

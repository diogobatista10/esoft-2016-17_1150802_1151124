/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.controller;

import java.util.List;
import project.model.Candidatura;
import project.model.CentroEventos;
import project.model.Evento;

/**
 *
 * @author diogo
 */
public class CriarCandidaturaController {

    private CentroEventos ce;
    private Candidatura c;
    private Evento e;

    /**
     * Construtor vazio
     */
    public CriarCandidaturaController() {
    }

    /**
     * Construtor completo
     *
     * @param ce Centro Eventos
     * @param c Candidatura
     * @param e Evento
     */
    public CriarCandidaturaController(CentroEventos ce, Candidatura c, Evento e) {
        this.ce = ce;
        this.c = c;
        this.e = e;
    }

    /**
     * Retorna a lista de eventos
     *
     * @return lista eventos
     */
    public List<Evento> getListaEventos() {
        return ce.getListaEventos();
    }

    /**
     * Cria uma nova candidatura
     *
     * @return nova candidatura
     */
    public Candidatura novaCandidatura() {
        return e.novaCandidatura();
    }

    /**
     * Modifica os dados da candidatura
     */
    public void setDados() {
        c.setDados();
    }

    /**
     * Valida a candidatura
     *
     * @return true caso valide
     */
    public boolean validaCandidatura() {
        return e.validaCandidatura(c);
    }

    /**
     * Regista a candidatura
     *
     * @return true caso registada
     */
    public boolean registaCandidatura() {
        return e.registaCandidatura(c);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.controller;

import java.util.List;
import project.model.CentroEventos;
import project.model.Evento;
import project.model.FAE;
import project.model.Organizador;
import project.model.Utilizador;

/**
 *
 * @author diogo
 */
public class DefinirFAEController {

    private CentroEventos ce;
    private Organizador o;
    private Evento e;
    private FAE fae;

    /**
     * Construtor completo
     *
     * @param ce Centro Evento
     * @param o Organizador
     * @param e Evento
     * @param fae FAE
     */
    public DefinirFAEController(CentroEventos ce, Organizador o, Evento e, FAE fae) {
        this.ce = ce;
        this.o = o;
        this.e = e;
        this.fae = fae;
    }

    /**
     * Construtor vazio
     */
    public DefinirFAEController() {
    }

    /**
     * Retorna lista de eventos do organizador
     *
     * @return lista eventos
     */
    public List<Evento> getListaEventosOrganizador() {
        return ce.getListaEventosOrganizador(o);
    }

    /**
     * Seleciona o evento
     */
    public void selecionaEvento() {
        ce.selecionaEvento(e);
    }

    /**
     * retorna lista Utilizadores
     *
     * @return lista utilizadore
     */
    public List<Utilizador> getListaUtilizadores() {
        return ce.getListaUtilizadores();
    }

    /**
     * Adiciona o fae ao evento
     */
    public void addFAE() {
        e.addFAE(fae);
    }

    /**
     * regista fae ao evento
     *
     * @return true caso registado
     */
    public boolean registaFAE() {
        if (!e.validaFAE(fae)) {
            return false;
        }
        addFAE();
        return true;
    }
}

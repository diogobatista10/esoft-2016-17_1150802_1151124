/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.controller;

import java.util.List;
import project.model.Candidatura;
import project.model.CentroEventos;
import project.model.Decisao;
import project.model.Evento;
import project.model.Utilizador;

/**
 *
 * @author diogo
 */
public class DecidirCandidaturasController {

    private CentroEventos ce;
    private Utilizador u;
    private Evento e;
    private Candidatura c;
    private Decisao d;

    /**
     * Construtor completo
     *
     * @param ce Centro Eventos
     * @param u Utilizador
     * @param e Evento
     * @param c Candidatura
     * @param d Decisão
     */
    public DecidirCandidaturasController(CentroEventos ce, Utilizador u, Evento e, Candidatura c, Decisao d) {
        this.ce = ce;
        this.u = u;
        this.e = e;
        this.c = c;
        this.d = d;
    }

    /**
     * Construtor vazio
     */
    public DecidirCandidaturasController() {
    }

    /**
     * Retorna lista eventos do fae
     *
     * @return lista eventos do fae
     */
    public List<Evento> getlistaEventosFAE() {
        return ce.getListaEventosFAE(u);
    }

    /**
     * Seleciona evento
     *
     * @return evento
     */
    public Evento selecionaEvento() {
        return ce.selecionaEvento(e);
    }

    /**
     * Retorna lista candidaturas por decidir
     *
     * @return lista candidaturas por decidir
     */
    public List<Candidatura> getListaCandidaturasPorDecidir() {
        return e.getListaCandidaturasPorDecidir(e);
    }

    /**
     * Seleciona candidatura
     *
     * @return candidatura
     */
    public Candidatura selecionaCandidatura() {
        return e.selecionaCandidatura(c);
    }

    /**
     * Retorna informação da candidatura
     *
     * @return info candidatura
     */
    public String getInfoCandidatura() {
        return e.getInformacaoCandidatura(c);
    }

    /**
     * Modifica a decisão
     *
     * @param d decisão
     * @param textoJustificativo textojustificativo
     */
    public void setDecisao(Boolean d, String textoJustificativo) {
        d = this.d.setDecisaoSN(true);
        textoJustificativo = this.d.getTextoJustificativo();
    }

    /**
     * Regista a decisão
     *
     * @return true caso registada
     */
    public boolean RegistaDecisao() {
        return e.registaDecisao();
    }
}

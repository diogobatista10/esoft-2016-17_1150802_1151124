/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.controller;

import java.util.List;
import project.model.*;

/**
 *
 * @author diogo
 */
public class CriarEventoController {

    private CentroEventos ce;
    private Evento e;

    /**
     * Construtor vazio
     */
    public CriarEventoController() {

    }

    /**
     * Construtor completo
     *
     * @param ce Centro Evento
     * @param e Evento
     */
    public CriarEventoController(CentroEventos ce, Evento e) {
        this.ce = ce;
        this.e = e;
    }

    /**
     * Cria um novo evento
     *
     * @return novo evento
     */
    public Evento novoEvento() {
        return ce.novoEvento();
    }

    /**
     * Modifica os dados do evento
     *
     * @param titulo titulo
     * @param textoDescritivo texto descritivo
     * @param dataInicio data inicio
     * @param dataFim data fim
     * @param local local
     */
    public void setDados(String titulo, String textoDescritivo, String dataInicio, String dataFim, Local local) {
        e.setTitulo(titulo);
        e.setTextoDescritivo(textoDescritivo);
        e.setDataInicio(dataInicio);
        e.setDataFim(dataFim);
        e.setLocal(local);

    }

    /**
     * Retorna a lista de utilizadores
     *
     * @return lista utilizadores
     */
    public List<Utilizador> getListaUtilizadores() {
        return ce.getListaUtilizadores();
    }

    /**
     * Retorna a lista de utilizadores por confirmar
     *
     * @return lista utilizadores por confirmar
     */
    public List<Utilizador> getListaUtilizadoresPorConfirmar() {
        return ce.getListaUtilizadoresPorConfirmar();
    }

    /**
     * Regista o organizador
     *
     * @param o organizador
     */
    public void RegistaOrganizador(Organizador o) {
        e.RegistaOrganizador(o);
    }

    /**
     * Valida o evento
     *
     * @return true se válido
     */
    public boolean validaEvento() {
        return ce.validaEvento(e);
    }

    /**
     * Regista evento
     *
     * @return true se registado
     */
    public boolean registaEvento() {
        if (!validaEvento()) {
            return false;
        }
        ce.registaEvento(e);
        return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.model;

/**
 *
 * @author diogo
 */
public class Decisao {

    private boolean decisao;
    private String textoJustificativo;
    private Candidatura c;

    /**
     * construtor vazio
     */
    public Decisao() {
    }

    /**
     * Retorna informação da candidatura
     *
     * @return informação da candidatura
     */
    public String getInfoCandidatura() {
        return c.toString();
    }

    /**
     * Retorna o texto justificativo da decisão
     *
     * @return texto justificativo
     */
    public String getTextoJustificativo() {
        return textoJustificativo;
    }

    /**
     * Modifica o texto justificativo da decisão
     *
     * @param textoJustificativo texto justificativo
     */
    public void setTextoJustificativo(String textoJustificativo) {
        if (textoJustificativo == null || textoJustificativo.trim().isEmpty()) {
            throw new IllegalArgumentException("Texto justificativo é inválido!");
        }
        this.textoJustificativo = textoJustificativo;
    }

    /**
     * Valida a decisão
     *
     * @return true caso seja válida ou false
     */
    public boolean valida() {
        throw new UnsupportedOperationException("Not supported yet"); //TODO
    }

    /**
     * Modifica a decisão
     *
     * @param decisao decisao
     * @return true ou false consoante a decisão
     */
    public boolean setDecisaoSN(boolean decisao) {
        return this.decisao = decisao;
    }

    /**
     * Modifica se a candidatura foi decidida ou não
     *
     * @return true caso sim ou false
     */
    public boolean setDecidida() {
        if (!valida() || getTextoJustificativo() == null || getTextoJustificativo().trim().isEmpty()) {
            return false;
        }
        return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author diogo
 */
public class Evento {

    private List<Organizador> listaOrganizadores = new ArrayList();
    private List<FAE> listaFAEs = new ArrayList();
    private List<Candidatura> listaCandidaturas = new ArrayList();
    private List<Candidatura> listaCandidaturasPorDecidir = new ArrayList();

    private String titulo;
    private String textoDescritivo;
    private String dataInicio;
    private String dataFim;
    private Local local;

    private Candidatura c;
    private Decisao d;

    /**
     * Construtor completo
     *
     * @param titulo titulo
     * @param textoDescritivo texto descritivo
     * @param dataInicio data início
     * @param dataFim data fim
     * @param local local
     */
    public Evento(String titulo, String textoDescritivo, String dataInicio, String dataFim, Local local) {
        this.titulo = titulo;
        this.textoDescritivo = textoDescritivo;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.local = local;
    }

    /**
     * construtor vazio
     */
    public Evento() {

    }

    /**
     * Retorna o título do evento
     *
     * @return título
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * Modifica o título do evento
     *
     * @param titulo título
     */
    public void setTitulo(String titulo) {
        if (titulo == null || titulo.trim().isEmpty()) {
            throw new IllegalArgumentException("Titulo é inválido!");
        }
        this.titulo = titulo;
    }

    /**
     * Retorna o texto descritivo do evento
     *
     * @return texto descritivo
     */
    public String getTextoDescritivo() {
        return textoDescritivo;
    }

    /**
     * Modifica o texto descritivo do evento
     *
     * @param textoDescritivo texto descritivo
     */
    public void setTextoDescritivo(String textoDescritivo) {
        if (textoDescritivo == null || textoDescritivo.trim().isEmpty()) {
            throw new IllegalArgumentException("Texto descritivo é inválido!");
        }
        this.textoDescritivo = textoDescritivo;
    }

    /**
     * Retorna a data de início do evento
     *
     * @return data início
     */
    public String getDataInicio() {
        return dataInicio;
    }

    /**
     * Modifica a data de início do evento
     *
     * @param dataInicio data inicio
     */
    public void setDataInicio(String dataInicio) {
        if (dataInicio == null || dataInicio.trim().isEmpty()) {
            throw new IllegalArgumentException("Data Inicio é inválida!");
        }
        this.dataInicio = dataInicio;
    }

    /**
     * Retorna a data de fim do evento
     *
     * @return data fim
     */
    public String getDataFim() {
        return dataFim;
    }

    /**
     * Modifica a data de fim do evento
     *
     * @param dataFim data fim
     */
    public void setDataFim(String dataFim) {
        if (dataFim == null || dataFim.trim().isEmpty()) {
            throw new IllegalArgumentException("Data Fim é inválida!");
        }
        this.dataFim = dataFim;
    }

    /**
     * Retorna o local
     *
     * @return local
     */
    public Local getLocal() {
        return local;
    }

    /**
     * modifica o local
     *
     * @param local local
     */
    public void setLocal(Local local) {
        this.local = local;
    }

    /**
     * Mostra a informação do evento
     *
     * @return info evento
     */
    @Override
    public String toString() {
        return "Evento{" + "titulo=" + titulo + ", textoDescritivo=" + textoDescritivo
                + ", dataInicio=" + dataInicio + ", dataFim=" + dataFim
                + ", local=" + local + '}';
    }

    /**
     * Valida os atributos do evento
     *
     * @return true ou false consoante é válido ou não
     */
    public boolean valida() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    //Organizador
    /**
     * Regista o organizador
     *
     * @param o organizador
     * @return true se registar ou false se não registar
     */
    public boolean RegistaOrganizador(Organizador o) {
        if (!validaOrganizador(o)) {
            return false;
        }
        addOrganizador(o);
        return true;
    }

    /**
     * Valida o organizador
     *
     * @param o organizador
     * @return true ou false consoante valida ou não o organizador
     */
    public boolean validaOrganizador(Organizador o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Adiciona o organizador à lista de organizadores
     *
     * @param o organizador
     */
    public void addOrganizador(Organizador o) {
        listaOrganizadores.add(o);
    }

    //FAE
    /**
     * Valida o fae
     *
     * @param fae fae
     * @return true ou false consoante valida ou não o fae
     */
    public boolean validaFAE(FAE fae) {
        return fae.valida();
    }

    /**
     * Adiciona o fae à lista de FAE's do evento
     *
     * @param fae fae
     */
    public void addFAE(FAE fae) {
        listaFAEs.add(fae);
    }

    /**
     * Regista o FAE
     *
     * @param fae fae
     * @return true se for registado
     */
    public boolean registaFAE(FAE fae) {
        if (!validaFAE(fae)) {
            return false;
        }
        addFAE(fae);
        return true;
    }
    //Decisão

    /**
     * Retorna lista Candidaturas por decidir
     *
     * @param e evento
     * @return lista candidaturas por decidir
     */
    public List<Candidatura> getListaCandidaturasPorDecidir(Evento e) {
        return e.listaCandidaturasPorDecidir;
    }

    /**
     * Seleciona uma candidatura
     *
     * @param c candidatura
     * @return candidatura
     */
    public Candidatura selecionaCandidatura(Candidatura c) {
        return c;
    }

    /**
     * Retorna a informação da candidaturra
     *
     * @param c candidatura
     * @return informação da candidatura
     */
    public String getInformacaoCandidatura(Candidatura c) {
        return d.getInfoCandidatura();
    }

    /**
     * Retorna a lista de candidaturas
     *
     * @return lista candidaturas
     */
    public List<Candidatura> getListaCandidaturas() {
        return this.listaCandidaturas;
    }

    /**
     * Retorna a lista de faes do evento
     *
     * @return lista faes
     */
    public List<FAE> getListaFAE() {
        return this.listaFAEs;
    }

    /**
     * Cria uma nova decisão
     *
     * @return nova decisão
     */
    public Decisao novaDecisao() {
        return new Decisao();
    }

    /**
     * Adiciona a decisão
     *
     * @param d decisão
     */
    public void addDecisao(Decisao d) {
        throw new UnsupportedOperationException("Not supported yet");
    }

    /**
     * Regista a decisão
     *
     * @return true se for registada
     */
    public boolean registaDecisao() {
        if (!validaDecisao(d)) {
            return false;
        }
        addDecisao(d);
        return true;
    }

    /**
     * Valida a decisão
     *
     * @param d decisão
     * @return true ou false se valida ou não
     */
    public boolean validaDecisao(Decisao d) {
        return d.valida();
    }

    /**
     * Cria uma nova candidatura
     *
     * @return nova candidatura
     */
    public Candidatura novaCandidatura() {
        return new Candidatura();
    }

    /**
     * Valida candidatura
     *
     * @param c candidatura
     * @return true se for válida ou false
     */
    public boolean validaCandidatura(Candidatura c) {
        return c.valida();
    }

    /**
     * Adiciona candidatura à lista de candidaturas por decidir
     *
     * @param c candidatura
     */
    public void addCandidatura(Candidatura c) {
        listaCandidaturasPorDecidir.add(c);
    }

    /**
     * Regista candidatura
     *
     * @param c candidatura
     * @return caso seja registada retorna true
     */
    public boolean registaCandidatura(Candidatura c) {
        if (!validaCandidatura(c)) {
            return false;
        }
        addCandidatura(c);
        return true;
    }
}

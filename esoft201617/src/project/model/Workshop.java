/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlo
 */
public class Workshop {
    
    
    
    private String codigo;
    private String descricaoo;
    private List<Tema> listaTema= new ArrayList();
    private List<Perito> listaPeritos = new ArrayList();
    public Workshop(String codigo, String descricaoo, Tema tema, Perito perito) {
        this.codigo = codigo;
        this.descricaoo = descricaoo;
        this.listaTema.add(tema);
        this.listaPeritos.add(perito);
    }
    public Workshop(){
    
    
    }
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricaoo() {
        return descricaoo;
    }

    public void setDescricaoo(String descricaoo) {
        this.descricaoo = descricaoo;
    }
    public Tema novoTema(){
    return new Tema();
    }
    
    public Perito novoPerito(){
    
    return new Perito();
    }
    public boolean addT(Tema e) {
        return listaTema.add(e);
    }

    public boolean addP(Perito e) {
        return listaPeritos.add(e);
    }

  

    public boolean setDados(String codigo, String descricao) {
      this.codigo=codigo;
      this.descricaoo=descricao;
      return true;
    }

    public boolean valida() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    
    
    
}

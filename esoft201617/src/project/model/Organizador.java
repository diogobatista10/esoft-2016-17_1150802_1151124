/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author diogo
 */
public class Organizador {

    private List<Evento> listaEventosOrganizador = new ArrayList();

    private Utilizador utilizador;

    public Organizador() {

    }

    /**
     *
     * @param utilizador
     */
    public void setUtilizador(Utilizador utilizador) {
        if (utilizador == null) {
            throw new IllegalArgumentException("Utilizador é inválido!");
        }
        this.utilizador = utilizador;
    }

    /**
     * retorna a lista de Eventos do Utilizador
     *
     * @return a lista de Eventos do utilizador que é organizador
     */
    public List<Evento> getListaEventosOrganizador() {
        return listaEventosOrganizador;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author diogo
 */
public class Utilizador {

    private List<Evento> listaEventosFAE = new ArrayList();

    private String nome;
    private String email;
    private String username;
    private String password;

    /**
     * Construtor vazio
     */
    public Utilizador() {

    }

    /**
     * Contrutor do Utilizador completo
     *
     * @param nome nome do utilizador
     * @param email email do utilizador
     * @param username username do utilizador
     * @param password password do utilizador
     */
    public Utilizador(String nome, String email, String username, String password) {

        setNome(nome);
        setEmail(email);
        setUsername(username);
        setPassword(password);
    }

    /**
     * Retorna nome do Utilizador
     *
     * @return nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Retorna email do Utilizador
     *
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Retorna username do Utilizador
     *
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Retorna password do Utilizador
     *
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Define o nome do Utilizador
     *
     * @param nome novo nome
     */
    public void setNome(String nome) {
        if (nome == null || nome.trim().isEmpty()) {
            throw new IllegalArgumentException("Nome é inválido!");
        }
        this.nome = nome;
    }

    /**
     * Define o email do Utilizador
     *
     * @param email email
     */
    public void setEmail(String email) {
        if (!email.matches(".+@.+\\..+")) {
            throw new IllegalArgumentException("Email inválido!");
        }
        this.email = email;
    }

    /**
     * Define o username do Utilizador
     *
     * @param username username
     */
    public void setUsername(String username) {
        if (username == null || username.trim().isEmpty()) {
            throw new IllegalArgumentException("Username é inválido!");
        }
        this.username = username;
    }

    /**
     * Define o password do Utilizador
     *
     * @param password password
     */
    public void setPassword(String password) {
        // Verifica se contém pelo menos 1 maiúscula e 1 minúscula
        if (!password.matches(".*[a-z].*") || !password.matches(".*[A-Z].*")) {
            throw new IllegalArgumentException("Password necessita letras maiúsculas e minúsculas!");
        }
        // Verifica se contém pelo menos 1 sinal de pontuação (.:,;-)
        if (!password.matches(".*[,\\.\\-:;].*")) {
            throw new IllegalArgumentException("Password necessita de pelo menos um sinal de pontuação (\".\", \":\", \",\", \";\" ou \"-\")!");
        }
        // Verifica se contém pelo menos 1 número
        if (!password.matches(".*[1-9].*")) {
            throw new IllegalArgumentException("Password necessita letras maiúsculas e minúsculas!");
        }

        this.password = password;
    }

    @Override
    public String toString() {
        return nome + " (" + username + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Utilizador other = (Utilizador) obj;
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        return true;
    }

    public int compareTo(Utilizador utilizador) {
        return nome.compareTo(utilizador.nome);
    }

    /**
     * Verifica se o username do Utilizador é igual
     *
     * @param username username a verificar
     * @return boolean true or false
     */
    public boolean hasUsername(String username) {
        return this.username.equalsIgnoreCase(username);
    }

    /**
     * valida utilizador
     *
     * @return true ou false consoante aceita ou não os dados do utilizador
     */
    public boolean valida() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Retorna a lista de eventos do FAE
     *
     * @return a lista de eventos do FAE
     */
    public List<Evento> getListaEventosFAE() {
        return listaEventosFAE;
    }

}

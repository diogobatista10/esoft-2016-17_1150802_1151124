/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author diogo
 */
public class CentroEventos {

    private List<Utilizador> listaUtilizadoresPorConfirmar = new ArrayList();
    private List<Utilizador> listaUtilizadores = new ArrayList();
    private List<Evento> listaEventos = new ArrayList();
    private List<Congresso> listaCongressos= new ArrayList();
    private List<Gestor> listaGestor= new ArrayList();

    //EVENTO
    /**
     * Cria um novo evento
     *
     * @return novo evento
     */
    public Evento novoEvento() {
        return new Evento();
    }

    /**
     * Valida o evento que foi criado
     *
     * @param e evento que foi criado
     * @return true ou false consoante é válido ou não
     */
    public boolean validaEvento(Evento e) {
        return e.valida();
    }

    /**
     * Regista o evento que foi criado na lista de Eventos caso seja válido
     *
     * @param e evento que foi criado
     * @return false se não for válido e true se for válido adicionando-o à
     * lista
     */
    public boolean registaEvento(Evento e) {
        if (!validaEvento(e)) {
            return false;
        }
        addEvento(e);
        return true;
    }

    /**
     * Adiciona o evento à lista de eventos
     *
     * @param e evento que foi criado
     */
    public void addEvento(Evento e) {
        listaEventos.add(e);
    }
//UTILIZADOR

    /**
     * Criar novo utilizador
     *
     * @return novo utilizador
     */
    public Utilizador novoUtilizador() {
        return new Utilizador();
    }

    /**
     * Valida o utilizador criado
     *
     * @param u utilizador que foi criado
     * @return true ou false consoante seja válido ou não
     */
    public boolean validaUtilizador(Utilizador u) {
        return u.valida();
    }

    /**
     * Regista o utilizador criado na lista de utilizadores por confirmar caso
     * passe a validação
     *
     * @param u utilizador que foi criado
     * @return false se o utilizador não foi válido e true se for válido
     * adicionando-o à lista de utilizadores
     */
    public boolean registaUtilizador(Utilizador u) {
        if (!validaUtilizador(u)) {
            return false;
        }
        addUtilizador(u);
        return true;
    }

    /**
     * Retorna o utilizador
     *
     * @param u utilizador
     * @return o utilizador
     */
    public Utilizador getUtilizador(Utilizador u) {
        return u;
    }

    /**
     * Confirma o utilizador que antes não estava confirmado e adiciona-o à
     * lista de utilizadores
     *
     * @param u utilizador
     */
    public void confirmarUtilizador(Utilizador u) {
        listaUtilizadores.add(u);
    }

    /**
     * Adiciona o utilizador à lista de utilizadores por confirmar
     *
     * @param u utilizador que foi criado
     */
    public void addUtilizador(Utilizador u) {
        listaUtilizadoresPorConfirmar.add(u);
    }

    /**
     * Retornar a lista de Utilizadores existentes
     *
     * @return lista utilizadores existentes
     */
    public List<Utilizador> getListaUtilizadores() {
        return listaUtilizadores;
    }

    /**
     * Retornar a lista de Utilizadores que ainda não foram confirmados
     *
     * @return lista de utilizadores não confirmados
     */
    public List<Utilizador> getListaUtilizadoresPorConfirmar() {
        return listaUtilizadoresPorConfirmar;
    }

    /**
     * Retornar a lista de Eventos de um Organizador
     *
     * @param o Organizador
     * @return lista de eventos do organizador
     */
    public List<Evento> getListaEventosOrganizador(Organizador o) {
        return o.getListaEventosOrganizador();
    }

    /**
     * Seleciona o evento desejado
     *
     * @param e Evento
     * @return evento selecionado
     */
    public Evento selecionaEvento(Evento e) {
        return e;
    }

    /**
     * Retornar a lista de eventos disponíveis
     *
     * @return lista de eventos disponíveis
     */
    public List<Evento> getListaEventos() {
        return listaEventos;
    }
    public List<Congresso> getListaCongressos(){
    
    return this.listaCongressos;
    }
    /**
     * Retorna a lista de Eventos de um FAE
     *
     * @param u utilizador
     * @return lista de Eventos do utilizador que é FAE
     */
    public List<Evento> getListaEventosFAE(Utilizador u) {
        return u.getListaEventosFAE();
    }

    public Gestor novoGestor(Utilizador u) {
    return new Gestor(u);   
    }
    public boolean registaGestor(Gestor g){
    
    return listaGestor.add(g);
    }
}

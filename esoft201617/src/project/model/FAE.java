/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.model;

/**
 *
 * @author diogo
 */
public class FAE {

    private Utilizador utilizador;

    /**
     * Construtor vazio
     */
    public FAE() {

    }

    /**
     *
     * @param utilizador
     */
    public void setUtilizador(Utilizador utilizador) {
        if (utilizador == null) {
            throw new IllegalArgumentException("Utilizador é inválido!");
        }
        this.utilizador = utilizador;
    }

    /**
     * Valida o utilizador que é FAE
     *
     * @return true ou false consoante é válido ou não
     */
    public boolean valida() {
        throw new UnsupportedOperationException("Not supported yet"); //TODO
    }
}

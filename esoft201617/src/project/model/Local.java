/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.model;

/**
 *
 * @author diogo
 */
public class Local {

    private String local;

    /**
     * Construtor
     *
     * @param local local
     */
    public Local(String local) {
        this.local = local;
    }

    /**
     * construtor vazio
     */
    public Local() {

    }

    /**
     * Retorna o local
     *
     * @return o local
     */
    public String getLocal() {
        return local;
    }

    /**
     * Modifica o local
     *
     * @param local
     */
    public void setLocal(String local) {
        if (local == null || local.trim().isEmpty()) {
            throw new IllegalArgumentException("Local é inválido!");
        }
        this.local = local;
    }

    /**
     * Mostra a informação do Local
     *
     * @return informação do local
     */
    @Override
    public String toString() {
        return "Local{" + "local=" + local + '}';
    }
}
